export type AuthService = {
	login(email: string, password: string): Promise<User>;
};
